<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>





		<div class="twelve columns">

							<div class="sponsored">
                            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<header class="entry-header">
		<?php //if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
		<div class="entry-meta">
			
		</div>
		

					<div class="v-data">



						<div class="tag-sponsored">sponsored</div>
						<!-- venue thumb --><div class="two columns thumb alpha"><?php the_post_thumbnail(); ?></div>
<?php
			//endif;

			
		?>
			<div class="seven columns venue-info">
           
							<h4><?php the_title(); ?></h4>			
			<?php the_excerpt(); ?>

							<div class="v-data-info"><!-- venue delivery info -->
								<table>
									<tr class="title_head">
									  <th>Cuisines</th>
									  <th>Estimated Time</th>
									  <th>Delivery Costs</th>
									</tr>
									<tr>
									  <td>Drink</td>
									  <td><?php echo get_post_meta( get_the_ID(), 'Estimated Time', true ); ?></td>
									  <td><?php echo get_post_meta( get_the_ID(), 'Delivery Costs', true ); ?></td>
									</tr>
								</table>
							</div></div>

						


						<!-- button to menu page --><div class="three columns order-btn omega">

							<div class=" proceed button">order now<a href=""></a></div>

							<p class="reviews-box">Quality <span>( <a href="">10 reviews</a> )</span></p>

							<div class="rating"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/menu-icons/rating.png" alt="" ></div>

						</div>


					<!-- ======= Venue Data ======= -->


					
</div><!-- ======= Venue Data ======= -->










<!-- ========= Rest Close ======== -->







<div class="clear"></div>


<div class="suggest-btn">

	<h4>Suggest a Restaurant</h4>

<div class="green button">click here</div>
</div>


		</div><!-- end twelve colom -->

			






































		</div>



		

	<div class="entry-summary">
		
	</div><!-- .entry-summary -->
	
	<div class="entry-content">
		<?php
			//the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ) );
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->
	

	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->