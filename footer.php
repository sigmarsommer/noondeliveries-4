﻿<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * @Author: MV Tech Help	
 */
?>




<!-- #content -->
  <div class="wave-second-a"></div>
</div>
<!-- // Home contact section // -->
<div class="home-contact-section">
  <div class="container">
    <div class="sixteen columns"> <span>If your're a restaurant, work with us to create a service your customers are hungry for!</span>
      <div class="contact button"><a href="<?php bloginfo('url');?>/contact-us/">Contact us</a></div>
    </div>
  </div>
</div>

<!-- // Footer Wrapper // -->
<div class="footer-wrapper">
<div class="waves"></div>
<div class="footer-navigation-section">
  <div class="container">
    <div class="sixteen columns">
      <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'footer-top-nav' ) ); ?>
      <div class="footer-social-links">
        <ul>
          <li><a href="#"><img src="<?php bloginfo('template_url');?>/images/layout-img/social-icon-fb.png" alt="noon deliveries" ></a></li>
          <li><a href="#"><img src="<?php bloginfo('template_url');?>/images/layout-img/social-icon-tw.png" alt="noon deliveries" ></a></li>
          <li><a href="#"><img src="<?php bloginfo('template_url');?>/images/layout-img/social-icon-gplus.png" alt="noon deliveries" ></a></li>
          <li><a href="#"><img src="<?php bloginfo('template_url');?>/images/layout-img/social-icon-yt.png" alt="noon deliveries" ></a></li>
        </ul>
      </div>
    </div>
    <div class="sixteen columns">
      <hr>
    </div>
    <!-- scheiding line --> 
    
  </div>
</div>
<div class="bottom-footer">
<div class="container">
  <div class="sixteen columns">
    <div class="promo-banner"><img src="<?php echo get_template_directory_uri(); ?>/images/layout-img/banner.jpg" alt="noondilevery banner"></div>
  </div>
  <!-- #primary -->

</div>
<!-- #main-content -->




		
				<div class="copyrights"><p>©2014 Noondeliveries.com - The food delivery site for the morning, noon, afternoon and night</p></div>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<!-- header tab search option -->
	<script>
			// Wait until the DOM has loaded before querying the document
			$(document).ready(function(){
				$('ul.tabs').each(function(){
					// For each set of tabs, we want to keep track of
					// which tab is active and it's associated content
					var $active, $content, $links = $(this).find('a');

					// If the location.hash matches one of the links, use that as the active tab.
					// If no match is found, use the first link as the initial active tab.
					$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
					$active.addClass('active');

					$content = $($active[0].hash);

					// Hide the remaining content
					$links.not($active).each(function () {
						$(this.hash).hide();
					});

					// Bind the click event handler
					$(this).on('click', 'a', function(e){
						// Make the old tab inactive.
						$active.removeClass('active');
						$content.hide();

						// Update the variables with the new link and content
						$active = $(this);
						$content = $(this.hash);

						// Make the tab active.
						$active.addClass('active');
						$content.show();

						// Prevent the anchor's default click action
						e.preventDefault();
					});
				});
			});
		</script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.cslider.js"></script>
		<script type="text/javascript">
			$(function() {
			
				$('#da-slider').cslider({
					bgincrement	: 0
				});
			
			});
		</script>	


	<?php wp_footer(); ?>
</body>
</html>