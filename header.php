<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>


    
    <!-- header search option -->
	<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
    </script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
    
    
    
    <!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/base.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/skeleton.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/layout.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/rest-list.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.28468.js"></script>
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script type="text/javascript" src="jquery.autofix_anything.js"></script>


<!-- fixed sidebar plugin -->
  <script>
	  $(document).ready( function() {
	     $(".sidebar1").autofix_anything({customOffset: true});
  	   $(".center-content-wrapper .header1").autofix_anything();
	  });
		
	</script>
	
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src='<?php echo get_template_directory_uri(); ?>/js/stickyfloat.js'></script><!-- order list sidebar -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function(e){
	$('.item').click(function (e){
		if($(this).next('.item-data').css('display') != 'block'){
			$('.active').slideUp('fast').removeClass('active');
			$(this).next('.item-data').addClass('active').slideDown('slow');
		} else {
			$('.active').slideUp('fast').removeClass('active');
		}
	});
});
</script>
		
		<noscript>
			<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/nojs.css" />
		</noscript>
	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114x114.png">
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div><!-- facebook like box script-->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="top-nav">
		<div class="container">
			<div class="nine columns logo">
			
				<h1><a href="">Noon Deliveries</a></h1>
				<p><strong>Save time</strong> order food online!</p>
			</div>
			<div class="seven columns account-options">
				<ul>
						<li class="reorder-option"><a href="http://noondeliveries.123webbusiness.com/en/re-order-2/">re-order</a></li>
						<li class="sign-up-option"><div class="sign button"><a href="http://noondeliveries.123webbusiness.com/my-account/">Sign Up</a></div></li>
						<li class="login-option"><a href="http://noondeliveries.123webbusiness.com/my-account/">Sign in</a></li>
					</ul>
			</div>
		</div>
	</div>
	<div class="waves"></div>
<!-- // Header // -->

	</header><!-- #masthead -->

	