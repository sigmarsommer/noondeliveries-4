<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="banner">
<?php echo do_shortcode('[slideshow]'); ?>
</div>
<div class="header">
		<div class="container">

				<div class="eight columns"><!-- tab section -->

					<div class="search-elements-front">

						<ul class='tabs'><div class="tabs-center-links">

						    <li class-"none-side"><a href='#tab1'>Districts</a></li><!--
						    --><li class="center-tab-home"><a href='#tab2'>Cuisines</a></li><!--
						    --><li><a href='#tab3'>Dishes</a></li>

						</div></ul>

						<div class="tab-content"><div class="center-tab-content">

						  <div id='tab1'>

						  	<h3>Enter here your district:</h3>

						    	<form class="form-wrapper"><input type="text" id="search" placeholder="i.e. Punda ..." required><input type="submit" value="ENTER" id="submit"></form>

								<div class="social-likebox">

									<ul>

										<li class="facebook-share">
											<div class="fb-follow" data-href="https://www.facebook.com/zuck" data-colorscheme="light" data-layout="standard" data-show-faces="true"></div>
										</li>
										<!-- twitter share -->
										<li class="twitter-share">

											<a href="https://twitter.com/Sigmarsommer" class="twitter-follow-button" data-show-count="false">Follow @Sigmarsommer</a>
												<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
										</li>
										<!-- google share -->
										<li class="googleplus-share">
 												<g:plusone></g:plusone>

										</li>

									</ul>

								</div>

						  </div>

						  <div id='tab2'>

						    <h3>The cuisine you're hungry for:</h3>

						    	<form class="form-wrapper">
								    <input type="text" id="search" placeholder="i.e. Kriyoyo ..." required>
								    <input type="submit" value="ENTER" id="submit">
								</form>



									<div class="social-likebox">

									<ul>

										<li class="facebook-share">
											<div class="fb-follow" data-href="https://www.facebook.com/zuck" data-colorscheme="light" data-layout="standard" data-show-faces="true"></div>
										</li>
										<!-- twitter share -->
										<li class="twitter-share">

											<a href="https://twitter.com/Sigmarsommer" class="twitter-follow-button" data-show-count="false">Follow @Sigmarsommer</a>
												<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
										</li>
										<!-- google share -->
										<li class="googleplus-share">
 												<g:plusone></g:plusone>

										</li>

									</ul>

								</div>

						  </div>
						  <div id='tab3'>

						    <h3>Dishes you like to eat:</h3>

						    	<form class="form-wrapper">
								    <input type="text" id="search" placeholder="i.e. Chicken ..." required>
								    <input type="submit" value="ENTER" id="submit">
								</form>

									<div class="social-likebox">

									<ul>

										<li class="facebook-share">
											<div class="fb-follow" data-href="https://www.facebook.com/zuck" data-colorscheme="light" data-layout="standard" data-show-faces="true"></div>
										</li>
										<!-- twitter share -->
										<li class="twitter-share">

											<a href="https://twitter.com/Sigmarsommer" class="twitter-follow-button" data-show-count="false">Follow @Sigmarsommer</a>
												<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
										</li>
										<!-- google share -->
										<li class="googleplus-share">
 												<g:plusone></g:plusone>

										</li>

									</ul>

								</div></div></div></div><!-- end tab-content -->
					</div>
				</div>

				<div class="eight columns slogan">

					<h2>start taking<br> online orders today</h2>

				</div>
		</div>
	</div>
	<div class="wave-second"></div>

<!-- // Center Content Wrapper // -->

	<div class="center-content-wrapper">
			<div class="firts-content-section"><!-- start how it works -->

				<div class="container">

					<div class="sixteen columns title_blue"><h2>How it works?</h2></div>

						<div class="one-third column search"><h1>Search</h1><p>Enter your location and find your favorite restaurants</p></div>

						<div class="one-third column order"><h1>Order</h1><p>Order the food you already love</p></div>

						<div class="one-third column eat"><h1>Eat</h1><p>Pay with cash. Receive food and enjoy!</p></div>
				</div>

			</div>
			<div class="second-content-section">
<!-- // What do they say  // -->
				<div class="container">

					<div class="sixteen columns title_blue spc"><h2>What do they say?</h2></div>

					<?php
query_posts('cat=2');
while (have_posts()) : the_post();
?>

	<div class="seven columns vid-one ">

<h1><?php the_title(); ?></h1>
<div class="videoWrapper">

						<div class="clearfix:before"></div>
					   <?php echo do_shortcode('[featured-video-plus]'); ?>
					</div>
<?php the_content(); ?></div><div class="two columns"><br></div>
<?php
endwhile;
?>
</div>
</div>

<div class="testimonial-section">


		<div id="da-slider" class="da-slider">
		<?php
query_posts('cat=3');
while (have_posts()) : the_post();
?>
				<div class="da-slide">
					<div class="da-img"><?php the_post_thumbnail(); ?></div>
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>

					</div><?php
endwhile;
?>


				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
			</div>


<div class="testimonials-second">

	<div class="container">
	<?php
$args = array( 'post_type' => 'restaurant', 'posts_per_page' => 3, 'order'=>'ASC' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
?>
		<div class="one-third column">
			<div class="rest-img"><?php the_post_thumbnail(); ?></div>
			<!-- // here (line 224: you have to put the imgIDs of the post and also an "if-statement" for when there is not pasphoto. // -->
			<div class="client-icon"><img src="<?php echo get_post_meta( get_the_ID(), 'passphoto', true ); ?>" alt="noon delivery"></div>
			
			<div class="client-testimonial">
			<span><?php the_title(); ?></span>

				<?php
				the_content(); ?>
			</div>
		</div><!-- -->
<?php
endwhile;
?>



	</div>

</div>

<div class="container">

<div class="facts">


		<div class="four columns ">
			<?php
		$args=array(
'post_type' => 'dishes',
'post_status' => 'publish'
);
$poststocount=get_posts($args);
?>
			<span><?php if($args){echo $post = count($poststocount);}
			else{
			echo "0";
			}
			?>
			</span>
			<p>Tasteful dishes</p>
		</div>

		<div class="four columns ">
		<?php
		$args=array(
'post_type' => 'restaurant',
'post_status' => 'publish'
);
$poststocount=get_posts($args);
?>
			<span><?php 
			
			echo $post = count($poststocount); 
			
			?></span>
			<p>Happily satisfied restaurants</p>
		</div>

		<div class="four columns ">
		<?php
		$args=array(
'post_type' => 'customer',
'post_status' => 'publish'
);
$poststocount=get_posts($args);
?>
			<span><?php if($args){echo $post = count($poststocount);}
			else{
			echo "0";
			}
			?>
			</span>
			<p>Happy customers</p>
		</div>

		<div class="four columns ">
		


			<span>24</span>
			<p>Hours open</p>
		</div>


	</div>

</div>



</div><!-- end testimonials -->

<div class="special_deals">

	<div class="container">

		<div class="sixteen columns">

			<div class="title_blue spc"><h2>Special Deals</h2></div>


			<div class="content-top">

<ul>
<?php
$args = array( 'post_type' => 'special', 'posts_per_page' => 3 );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
?>
            			<li><?php the_post_thumbnail(); ?>
            			<span class="discount"><?php echo get_post_meta( get_the_ID(), 'discount', true ); ?></span>
            			<span class="price"><?php echo get_post_meta( get_the_ID(), 'price', true ); ?></span>
            			<h4><?php the_title(); ?></h4>
            			<?php the_content(); ?>
            			<a href="" class="add_to_order"><p>Add to your order list</p></a></li>
            			<?php endwhile; ?></ul>
            	</div>

            	<div class="proceed button deals"><a href="">See more special deals</a></div>


		</div>


	</div>


</div>




		<div class="wave-second-a"></div>
	</div>
<!-- // Home contact section // -->
	<div class="home-contact-section">
		<div class="container">
			<div class="sixteen columns">
				<span>If your're a restaurant, work with us to create a service your customers are hungry for!</span>

				<div class="contact button"><a href="">Contact us</a></div>
			</div>
	</div>
	</div>

<!-- // Footer Wrapper // -->
	<div class="footer-wrapper">
		<div class="waves"></div>
		<div class="footer-navigation-section">
			<div class="container">

				<div class="sixteen columns">

				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'footer-top-nav' ) ); ?>

				<div class="footer-social-links">

					<?php if ( function_exists('cn_social_icon') ) echo cn_social_icon(); ?>
				</div>

			</div>

			<div class="sixteen columns"><hr></div><!-- scheiding line -->

			<div class="bottom-browse-nav">

			<div class="four columns">
					<h4>Browse by district:</h4>
					<table>
						<tbody>
							<tr>
								<td><a href="">A</a></td>
								<td><a href="">N</a></td>

							</tr>
							<tr>
								<td><a href="">B</a></td>
								<td><a href="">O</a></td>

							</tr>
							<tr>
								<td><a href="">C</a></td>
								<td><a href="">P</a></td>

							</tr>
							<tr>
								<td><a href="">D</a></td>
								<td><a href="">Q</a></td>

							</tr>
							<tr>
								<td><a href="">E</a></td>
								<td><a href="">R</a></td>

							</tr>
							<tr>
								<td><a href="">F</a></td>
								<td><a href="">S</a></td>

							</tr>
							<tr>
								<td><a href="">G</a></td>
								<td><a href="">T</a></td>

							</tr>
							<tr>
								<td><a href="">H</a></td>
								<td><a href="">U</a></td>

							</tr>
							<tr>
								<td><a href="">I</a></td>
								<td><a href="">V</a></td>

							</tr>
							<tr>
								<td><a href="">J</a></td>
								<td><a href="">W</a></td>

							</tr>
							<tr>
								<td><a href="">K</a></td>
								<td><a href="">X</a></td>

							</tr>
							<tr>
								<td><a href="">L</a></td>
								<td><a href="">Y</a></td>

							</tr>
							<tr>
								<td><a href="">M</a></td>
								<td><a href="">Z</a></td>

							</tr>

						</tbody>
					</table>
				</div>

				<div class="four columns">
					<h4>Browse by cuisine:</h4>

					<ul>
						<li><a href="">Kriyoyo Delivery</a></li>
						<li><a href="">Chinese Delivery</a></li>
						<li><a href="">Halal Delivery</a></li>
						<li><a href="">Indian Delivery</a></li>
						<li><a href="">Italian Delivery</a></li>
						<li><a href="">Japanese Delivery</a></li>
						<li><a href="">Pizza Delivery</a></li>
						<li><a href="">Barbecue Delivery</a></li>
						<li><a href="">Asian Delivery</a></li>
						<li><a href="">Other Deliveries</a></li>
					</ul>

				</div>

				<div class="four columns">

					<h4>Browse by dish:</h4>

						<ul>
							<li><a href="">BBQ Ribs</a></li>
							<li><a href="">Butter Chicken</a></li>
							<li><a href="">Chicken Korma</a></li>
							<li><a href="">Chicken Tikka Masala</a></li>
							<li><a href="">Green Curry</a></li>
							<li><a href="">Honey Chicken</a></li>
							<li><a href="">Kung Po Chicken</a></li>
							<li><a href="">Margherita Pizza</a></li>
							<li><a href="">Tono Pizza</a></li>
							<li><a href="">Tandoori Chicken</a></li>
							<li><a href="">Beef A La Paria</a></li>
							<li><a href="">Kabritu Stoba</a></li>
							<li><a href="">Pasta</a></li>

						</ul>

				</div>

				<div class="four columns">

					<h4>New Restaurants:</h4>

						<ul>
							<li><a href="">Zeera, Otrabanda</a></li>
							<li><a href="">Pizzaboy, Janwe</a></li>
							<li><a href="">Peri Peri Grill, Santa Maria</a></li>
							<li><a href="">Pizzaiolo Bargen, Muizenberg</a></li>
							<li><a href="">Snack XL, Punda</a></li>
						</ul>

				</div>

				</div><!-- end footer browse nav -->

			</div>
		</div>
		<div class="footer-newsletter-section">
			<div class="container">
				<div class="eight columns">

					<h4>Updates of Noondeliveries?</h4>

						<p>Subscribe to NoonDeliveries news, offers and promotions<br> from restaurants.
						You can unsubscribe anytime!</p>

				</div>

			<div class="eight columns">

				<?php echo do_shortcode('[simpleSubscribeForm]'); ?>
			</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="container">
				<div class="sixteen columns">

						<div class="promo-banner"><?php if ( function_exists( 'useful_banner_manager_banners' ) ) { useful_banner_manager_banners( '1', 1 ); } ?> </div>
<?php

include('footer2.php');?>